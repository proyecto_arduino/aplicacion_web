import { Injectable } from '@angular/core';
import { SocketService } from './socket.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(
    public socket : SocketService
  ) { }

  mensaje(msj){
    const payload = {
      cuerpo: msj
    }

    this.socket.emitir('mensaje', payload);
  }

  getMensaje(){
    return this.socket.listen('mensaje-nuevo');
  }
}
