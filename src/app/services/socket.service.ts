import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  public status:boolean = false;

  constructor(
    private socket:Socket
  ) {
    this.conexion();
   }

  conexion(){

    this.socket.on('connect', ()=>{
      console.log("Conectado");
      this.status = true;
    });

    this.socket.on('disconnect', ()=>{
      console.log("Desconectado");
      this.status = false;
    });

  }

  emitir(evento:string, payload?:any, callback?: Function){

    this.socket.emit(evento, payload, callback);

  }

  listen( evento:string ){

    return this.socket.fromEvent(evento);

  }

}
