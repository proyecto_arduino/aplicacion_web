import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-humedad-maxima',
  templateUrl: './humedad-maxima.component.html',
  styleUrls: ['./humedad-maxima.component.css']
})
export class HumedadMaximaComponent implements OnInit, OnChanges {

  @Input("fechahoramayor") public fechahora:any;
  @Input("humedadmayor") public humedad:any;
  public displayedColumns: string[] = ['humedad', 'fecha'];
  public dataSource:any;

  constructor() { }

  ngOnInit() {

  }
  
  ngOnChanges(){
    this.dataSource = [{
      humedad: this.humedad,
      fecha: this.fechahora
    }];
  }

}
