import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-humedad-minima',
  templateUrl: './humedad-minima.component.html',
  styleUrls: ['./humedad-minima.component.css']
})
export class HumedadMinimaComponent implements OnInit, OnChanges {

  @Input("fechahoramenor") public fechahora:any;
  @Input("humedadmenor") public humedad:any;
  public displayedColumns: string[] = ['humedad', 'fecha'];
  public dataSource:any;

  constructor() { }

  ngOnInit() {

  }

  ngOnChanges(){
    this.dataSource = [{
      humedad: this.humedad,
      fecha: this.fechahora
    }];
  }
  
}