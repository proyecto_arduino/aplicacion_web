import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { SocketService } from 'src/app/services/socket.service';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public humedad:number = 0;
  public humedadmenor:number = 100;
  public humedadmayor:number = 0;
  public fechahoramenor:any;
  public fechahoramayor:any;
  public fechahoactual:any;
  public canvas:boolean=true;
  
  constructor(
    public socket: SocketService,
    public chatService: ChatService
  ) { }

  ngOnInit() {
    this.chatService.mensaje('mensaje desde angular');

    this.chatService.getMensaje().subscribe( (data:any) => {
      let now = moment();
      let sacaT = now.format().split('T');
      this.fechahoactual = sacaT[0] + '  ' +  sacaT[1].split('-')[0];
      this.humedad = Number(data);
      if(this.humedad > this.humedadmayor){
        this.humedadmayor = this.humedad;
        this.fechahoramayor = this.fechahoactual;
      }
      if(this.humedadmenor > this.humedad){
        this.humedadmenor = this.humedad;
        this.fechahoramenor = this.fechahoactual;
      }
      
    })
  }

  getTabGroup($event){
    if($event.index == 0){
      this.canvas = true;
    }else{
      this.canvas = false;
    }  
  }

}
