import { RouterModule, Routes, CanActivate } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';

const app_routes: Routes = [
  { path: 'home', component: HomeComponent, data: { expected: ['HOME'] } },
  { path: 'login', component: LoginComponent },
  { path: '**', pathMatch: 'full', redirectTo: '/login' }
];

export const APP_ROUTING = RouterModule.forRoot(app_routes, { useHash: true });
