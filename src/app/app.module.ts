import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { AppComponent } from './app.component';
import { environment } from 'src/environments/environment';
import { AngularMaterialModule } from './app.material';
import { LoginComponent } from './pages/login/login.component';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FooterComponent } from './pages/secciones/footer/footer.component';
import { ChartsModule } from 'ng2-charts';
import { HumedadMaximaComponent } from './pages/secciones/humedad-maxima/humedad-maxima.component';
import { HumedadMinimaComponent } from './pages/secciones/humedad-minima/humedad-minima.component';
import { AccionesComponent } from './pages/secciones/acciones/acciones.component';
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { APP_ROUTING } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

const config: SocketIoConfig = { url: environment.wsURL, options: {} };

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    HumedadMaximaComponent,
    HumedadMinimaComponent,
    AccionesComponent,
    HomeComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule, 
    ReactiveFormsModule,
    SocketIoModule.forRoot(config),
    ChartsModule,
    APP_ROUTING,
    HttpModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
