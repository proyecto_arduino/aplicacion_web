import { NgModule} from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material';
import { MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatTableModule} from '@angular/material/table';
import { MatCardModule} from '@angular/material/card';
import { MatDividerModule} from '@angular/material/divider';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { MatMenuModule} from '@angular/material/menu';
import { MatButtonModule} from '@angular/material/button';

@NgModule({
    exports:[
        MatTabsModule,
        MatIconModule,
        MatSlideToggleModule,
        MatTableModule,
        MatCardModule,
        MatDividerModule,
        MatFormFieldModule, 
        MatInputModule,
        MatMenuModule,
        MatButtonModule
    ],
    declarations:[

    ]
})

export class AngularMaterialModule {}